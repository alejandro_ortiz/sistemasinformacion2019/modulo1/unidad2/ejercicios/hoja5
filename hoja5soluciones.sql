﻿USE ciclistas;

-- 1. Nombre y edad de los ciclistas que NO han ganado etapas

  -- C1 - ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal 
    FROM etapa e;

  -- Consulta final
  SELECT c.nombre, c.edad 
    FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 2. Nombre y edad de los ciclistas que NO han ganado puertos

  -- C1 - ciclistas que han ganado puertos
  SELECT DISTINCT p.dorsal 
    FROM puerto p;

  -- Consulta final
  SELECT c.nombre, c.edad 
    FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 3. Listar el director de los equipos que tengan ciclistas que NO hayan ganado NINGUNA etapa

  -- C1 - Equipos con ciclistas que no han ganado etapas
  SELECT DISTINCT c.nomequipo 
    FROM ciclista c 
    LEFT JOIN etapa e1 
    ON c.dorsal = e1.dorsal 
    WHERE e1.dorsal IS NULL;

  -- Final
  SELECT e.director 
    FROM equipo e 
    JOIN (
      SELECT DISTINCT c.nomequipo 
        FROM ciclista c 
        LEFT JOIN etapa e1 
        ON c.dorsal = e1.dorsal 
        WHERE e1.dorsal IS NULL
    ) c1 
    ON e.nomequipo = c1.nomequipo; 

-- 4. Dorsal y nombre del ciclista que NO hayan llevado algún maillot

  -- C1 - ciclistas que han llevado algun maillot
  SELECT DISTINCT l.dorsal 
    FROM lleva l;

  -- final
  SELECT c.dorsal, c.nombre 
    FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT l.dorsal 
        FROM lleva l
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 5. Dorsal y nombre del ciclista que NO hayan llevado el maillot amarillo NUNCA

  -- C1 - ciclistas que han llevado el maillot amarillo
  SELECT DISTINCT c.dorsal 
    FROM ciclista c 
    JOIN lleva l 
    ON c.dorsal = l.dorsal 
    WHERE l.código='MGE';

  -- final 
  SELECT c.dorsal, c.nombre 
    FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT c.dorsal 
        FROM ciclista c 
        JOIN lleva l 
        ON c.dorsal = l.dorsal 
        WHERE l.código='MGE'
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 6. Indicar el numetapa de las etapas que NO tengan puertos

  -- C1 - etapas con puertos
  SELECT DISTINCT p.numetapa 
    FROM puerto p;

  -- final
  SELECT e.numetapa 
    FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 7. Indicar la distancia media de las etapas que NO tengan puertos

  -- C1 - etapas con puertos
  SELECT DISTINCT p.numetapa 
    FROM puerto p;

  -- final
  SELECT AVG(e.kms)distanciaMedia 
    FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 8. Listar el número de ciclistas que NO hayan ganado alguna etapa

  -- C1 - ciclistas que han ganado alguan etapa
  SELECT DISTINCT e.dorsal 
    FROM etapa e;

  -- Consulta final
  SELECT COUNT(*) numCiclistas 
    FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 9. Listar el dorsal de los ciclistas que hayan ganado alguna etapa que NO tenga puerto

  -- C1 - etapas con puertos
  SELECT DISTINCT p.numetapa 
    FROM puerto p;

  -- final
  SELECT DISTINCT e.dorsal 
    FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 10. Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que NO tengan puertos

  -- C1 - ciclistas que han ganado etapas
  SELECT e.dorsal, e.numetapa 
    FROM  etapa e 
    JOIN puerto p 
    ON e.numetapa = p.numetapa;

  -- Final
  SELECT DISTINCT e.dorsal 
    FROM etapa e 
    LEFT JOIN (
      SELECT e.dorsal, e.numetapa 
      FROM  etapa e 
      JOIN puerto p 
      ON e.numetapa = p.numetapa
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;